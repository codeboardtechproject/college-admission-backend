package com.example.demo.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Repository.StudentRegi;
import com.example.demo.Repository.UserRepository;
import com.example.demo.model.mongoData;
//import com.example.demo.Repository.UserRepository;
//import com.example.demo.Repository.UserRepository;
import com.example.demo.model.registration;
import com.example.demo.model.result;
//import com.example.demo.model.result;
import com.example.demo.service.StudentService;


@CrossOrigin(origins= "http://localhost:4200/")
@RestController

@RequestMapping("/student")
public class StudentController {
	@Autowired
	private StudentService studentService;
	
	@Autowired
	StudentRegi studentregi;
	
	
	@PostMapping("/registration")
	public registration addData(@RequestBody registration regi) {
		return studentService.addStudentService(regi);
	}

		@PostMapping("/login")
		public ResponseEntity<registration>login(@RequestBody registration regi){
			return studentService.loginUser(regi);
		}
	
	
		@GetMapping("/sem1/{email}")
		public List<Object> getSem1(@PathVariable("email")String email){
				int id=studentService.getData(email);
			List<Object> data =  studentService.getId(id);
			return data;
		}
		
		@GetMapping("/sem2/{email}")
		public List<Object> getSem2(@PathVariable("email")String email){
			int id=studentService.getData(email);			
			List<Object> data =  studentService.getId1(id);			
			return data;
		}
		
		
		@GetMapping("/sem3/{email}")
		public List<Object> getSem3(@PathVariable("email")String email){
			int id=studentService.getData(email);			
			List<Object> data =  studentService.getId2(id);			
			return data;
		}
		
		@GetMapping("/sem4/{email}")
		public List<Object> getSem4(@PathVariable("email")String email){
			int id=studentService.getData(email);			
			List<Object> data =  studentService.getId3(id);			
			return data;
		}
		
		@Autowired
		private KafkaTemplate<String,String> kafkaTemplate;
		
		@PostMapping("/gmail")
		public String publish(@RequestBody Map<String, Object> request) {
		  List<String> labels =  (List<String>) request.get("label");
		  List<String> data = (List<String>) request.get("data");

		  String publishMessage = constructPublishMessage(labels, data);

		  kafkaTemplate.send("semester", publishMessage);
		  return "Email published: " + publishMessage;
		}

		private String constructPublishMessage(List<String> labels, List<String> data) {
				
			if (labels == null || data == null) {
			    return "Labels or data is null";
			  }
			
		  StringBuilder sb = new StringBuilder();
		  for (int i = 0; i < labels.size(); i++) {
		    sb.append(labels.get(i)).append(":").append(data.get(i));
		    if (i < labels.size() - 1) {
		      sb.append(", ");
		    }
		  }
		  return sb.toString();
		}
		
       	
}
