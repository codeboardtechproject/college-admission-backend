package com.example.demo.model;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="student")
public class registration {
	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name="Id")
		private int id;
		
		@Column(name="firstname")
		private String firstName;
		
		@Column(name="lastname")
	    private String lastName;
		
		@Column(name="dob")
	    private String  dob;
		
		@Column(name="mobile")
	    private Long mobile;
		
		@Column(name="sslc")
	    private String sslc;
		
		@Column(name="hsc")
	    private String hsc;
		
		@Column(name="email")
	    private String email;
		
		@Column(name="password")
	    private String password;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getDob() {
			return dob;
		}

		public void setDob(String dob) {
			this.dob = dob;
		}

		public Long getMobile() {
			return mobile;
		}

		public void setMobile(Long mobile) {
			this.mobile = mobile;
		}

		public String getSslc() {
			return sslc;
		}

		public void setSslc(String sslc) {
			this.sslc = sslc;
		}

		public String getHsc() {
			return hsc;
		}

		public void setHsc(String hsc) {
			this.hsc = hsc;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public registration(int id, String firstName, String lastName, String dob, Long mobile, String sslc, String hsc,
				String email, String password) {
			super();
			this.id = id;
			this.firstName = firstName;
			this.lastName = lastName;
			this.dob = dob;
			this.mobile = mobile;
			this.sslc = sslc;
			this.hsc = hsc;
			this.email = email;
			this.password = password;
		}

		public registration() {
			super();
			// TODO Auto-generated constructor stub
		}

		
		
		
		
	    
}