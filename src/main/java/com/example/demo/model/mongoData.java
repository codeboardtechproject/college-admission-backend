package com.example.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.persistence.Id;
import lombok.Data;

@Data
@Document(collection="student")
public class mongoData {
	
		@Id
		private int sem;
		private int sendingMail;
		private int failedMail;
		public int getSem() {
			return sem;
		}
		public void setSem(int sem) {
			this.sem = sem;
		}
		public int getSendingMail() {
			return sendingMail;
		}
		public void setSendingMail(int sendingMail) {
			this.sendingMail = sendingMail;
		}
		public int getFailedMail() {
			return failedMail;
		}
		public void setFailedMail(int failedMail) {
			this.failedMail = failedMail;
		}
		public mongoData(int sem, int sendingMail, int failedMail) {
			super();
			this.sem = sem;
			this.sendingMail = sendingMail;
			this.failedMail = failedMail;
		}
		public mongoData() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		
}
