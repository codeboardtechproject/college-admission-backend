package com.example.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class result {
		
	@ManyToOne
	@JoinColumn(name="Std_id")
	private registration regi;
	
	@ManyToOne
	@JoinColumn(name="sub_id")
	private Subject sub;
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id")
		private int id;
		
		@Column(name = "sem1")
		private String sem1;
		
		@Column(name = "sem2")
		private String sem2;
		
		@Column(name = "sem3")
		private String sem3;
		
		@Column(name = "sem4")
		private String sem4;

		public registration getRegi() {
			return regi;
		}

		public void setRegi(registration regi) {
			this.regi = regi;
		}

		public Subject getSub() {
			return sub;
		}

		public void setSub(Subject sub) {
			this.sub = sub;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getSem1() {
			return sem1;
		}

		public void setSem1(String sem1) {
			this.sem1 = sem1;
		}

		public String getSem2() {
			return sem2;
		}

		public void setSem2(String sem2) {
			this.sem2 = sem2;
		}

		public String getSem3() {
			return sem3;
		}

		public void setSem3(String sem3) {
			this.sem3 = sem3;
		}

		public String getSem4() {
			return sem4;
		}

		public void setSem4(String sem4) {
			this.sem4 = sem4;
		}

		public result(registration regi, Subject sub, int id, String sem1, String sem2, String sem3, String sem4) {
			super();
			this.regi = regi;
			this.sub = sub;
			this.id = id;
			this.sem1 = sem1;
			this.sem2 = sem2;
			this.sem3 = sem3;
			this.sem4 = sem4;
		}

		public result() {
			super();
			// TODO Auto-generated constructor stub
		}

		

		
		
}
