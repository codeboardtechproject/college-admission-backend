package com.example.demo.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.spi.DirStateFactory.Result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import com.example.demo.Repository.StudentRegi;
import com.example.demo.Repository.UserRepository;

import com.example.demo.model.registration;
import com.example.demo.model.Subject;
import com.example.demo.model.result;

@Service
public class StudentService{
	@Autowired
	 private StudentRegi studentRegi;
	
	@Autowired
	private UserRepository userrepo;
	
	
	
	public int getData(String email ) {
		registration register=studentRegi.getByemail(email);
		int id=0;
		if(register!=null) {
			 id=register.getId();			
			 return id;
		}else {
			return 0;
		}
		
	}
	
	
	public registration addStudentService(registration regi) {
		
		return this.studentRegi.save(regi);
	}
	
	
	
	public ResponseEntity<registration> loginUser( registration regi){
		registration register=studentRegi.findByemail(regi.getEmail());
		if(register.getPassword().equals(regi.getPassword())) 
			return ResponseEntity.ok(register);
		else
				return (ResponseEntity<registration>) ResponseEntity.internalServerError();
	}
	
	
	public List<Object> getId(int id){
		return userrepo.getSem1(id);
	}
	
	public List<Object> getId1(int id){
		return userrepo.getSem2(id);
	}
	
	public List<Object> getId2(int id){
		return userrepo.getSem3(id);
	}
	
	public List<Object> getId3(int id){
		return userrepo.getSem4(id);
	}

	
	
	
}
