package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.result;

import com.example.demo.model.result;

public interface UserRepository extends JpaRepository<result,Integer>{
	

	@Query(value="Select s.sub_name as Subject,r.sem1 as Mark from result r,subject_details s ,student sd  where  sd.id=r.Std_id  and r.sub_id=s.id and sd.id=:c",nativeQuery=true)
	List<Object> getSem1(@Param("c")int id);
	
	
	@Query(value="Select s.sub_name as Subject,r.sem2 as Mark from result r,subject_details s ,student sd  where  sd.id=r.Std_id  and r.sub_id=s.id and sd.id=:c",nativeQuery=true)
	List<Object> getSem2(@Param("c")int id);
	
	@Query(value="Select s.sub_name as Subject,r.sem3 as Mark from result r,subject_details s ,student sd  where  sd.id=r.Std_id  and r.sub_id=s.id and sd.id=:c",nativeQuery=true)
	List<Object> getSem3(@Param("c")int id);
	
	@Query(value="Select s.sub_name as Subject,r.sem4 as Mark from result r,subject_details s ,student sd  where  sd.id=r.Std_id  and r.sub_id=s.id and sd.id=:c",nativeQuery=true)
	List<Object> getSem4(@Param("c")int id);
}
