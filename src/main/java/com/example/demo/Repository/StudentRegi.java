package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.registration;
import com.example.demo.model.result;

@Repository
public interface StudentRegi extends JpaRepository<registration,Integer>{
	
	registration findByemail(String email);
	
	registration getByemail(String email);
	
	
}
